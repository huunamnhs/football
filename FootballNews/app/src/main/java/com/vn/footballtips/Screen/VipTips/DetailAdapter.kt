package com.vn.footballtips.Screen.VipTips

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.example.MatchFootball
import com.example.example.Tips
import com.squareup.picasso.Picasso
import com.vn.footballtips.MainActivity
import com.vn.footballtips.R
import com.vn.footballtips.Utils.Utils

class DetailAdapter(val context: Context): RecyclerView.Adapter<DetailAdapter.ViewHolder>() {
    private var tips = ArrayList<Tips>()
    private var itemMatch: MatchFootball? = null
    private var matchId = ""
    private var mapItemUnLock = HashMap<String, String>()
    private var listener: VipListener? = null

    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: AppCompatTextView? = null
        var fee: AppCompatTextView? = null
        var selection: AppCompatTextView? = null
        var tvAccuracy: AppCompatTextView? = null
        var rateView: AppCompatTextView? = null
        var selectionView: View? = null
        var lockIconView: AppCompatImageView? = null

        init {
            tvName = itemView.findViewById(R.id.tv_tip_name)
            fee = itemView.findViewById(R.id.tv_fee)
            selection = itemView.findViewById(R.id.tv_selection)
            tvAccuracy = itemView.findViewById(R.id.tv_accuracy)
            rateView = itemView.findViewById(R.id.tv_rate)
//            lockIconView = itemView.findViewById(R.id.icon_lock)
            selectionView = itemView.findViewById(R.id.layout_selection)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_tips, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = tips.get(position)
        item.let {
            val unlock = matchId.plus(it.id)
            holder.tvName?.text = item.name
            if (it.coinfee != 0 && Utils.historyList.contains(unlock).not()) {
                holder.lockIconView?.visibility = View.VISIBLE
                holder.rateView?.visibility = View.GONE
                holder.selection?.visibility = View.GONE
                holder.tvAccuracy?.visibility = View.GONE
                holder.selectionView?.visibility = View.GONE

                holder.fee?.text = "Unlock: ${item.coinfee} Coin"
                holder.itemView.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(p0: View?) {
                        val tip = tips.get(holder.adapterPosition)
                        val unlockValue = tip.coinfee?:0
                        if (Utils.credits >= unlockValue) {
                            val strName = matchId.plus(tip.id)
                            animationDialog(context as MainActivity , unlockValue, strName)
                        } else {
                            (context as MainActivity).openIAP()
                        }
                    }

                })

            } else {
                holder.lockIconView?.visibility = View.GONE
                holder.rateView?.visibility = View.VISIBLE
                holder.selection?.visibility = View.VISIBLE
                holder.tvAccuracy?.visibility = View.VISIBLE
                holder.selectionView?.visibility = View.VISIBLE
                holder.rateView?.text = it.rate.toString()
                holder.selection?.text = it.selection
                holder.tvAccuracy?.text = it.accuracy.toString().plus("%")
                holder.fee?.text = "See More"
                holder.itemView.setOnClickListener(object : View.OnClickListener{
                    override fun onClick(view: View?) {
                        itemMatch?.let { it1 -> ViewAllDialog(context as MainActivity, it1, tips.get(holder.adapterPosition)) }
                    }
                })

            }
        }
    }

    override fun getItemCount(): Int {
        return tips.size
    }

    private fun ViewAllDialog(activity: Activity, item: MatchFootball, tip: Tips) {
        val dialog = Dialog(activity,R.style.DialogSlideUp)
        val lp = WindowManager.LayoutParams()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_free1, null)

        val tvLeage = view.findViewById<AppCompatTextView>(R.id.tv_leage)
        val tvDateTime = view.findViewById<AppCompatTextView>(R.id.tv_date_time)
        val tvScore = view.findViewById<AppCompatTextView>(R.id.tv_score)
        val tvRate = view.findViewById<AppCompatTextView>(R.id.tv_rate)
        val tvHome = view.findViewById<AppCompatTextView>(R.id.tv_home)
        val tvAway = view.findViewById<AppCompatTextView>(R.id.tv_away)
        val tvTipName = view.findViewById<AppCompatTextView>(R.id.tv_tip_name)
        val tvSelection = view.findViewById<AppCompatTextView>(R.id.tv_selection)
        val tvAccuracy = view.findViewById<AppCompatTextView>(R.id.tv_accuracy)
        val tvOpinion = view.findViewById<AppCompatTextView>(R.id.tv_opinion)
        val imgHome = view.findViewById<AppCompatImageView>(R.id.img_home)
        val imgAway = view.findViewById<AppCompatImageView>(R.id.img_away)
        val imgBack = view.findViewById<AppCompatImageView>(R.id.img_back)

        tvLeage.text = item.leage
        tvDateTime.text = item.time
        tvScore.text = item.finalScore
        tvHome.text = item.home
        tvAway.text = item.away
        Picasso.get()
            .load(item.homeFlag)
            .into(imgHome)
        Picasso.get()
            .load(item.awayFlag)
            .into(imgAway)
        tip.let {
            tvRate.text = it.rate.toString()
            tvTipName.text = it.name
            tvSelection.text = it.selection
            tvAccuracy.text = it.accuracy.toString().plus("%")
            tvOpinion.text = it.expertOpinion
        }
        imgBack.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                dialog.dismiss()
            }
        })
        dialog.setContentView(view)
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        val outMetrics = DisplayMetrics()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            val display = activity.display
            display?.getRealMetrics(outMetrics)
        } else {
            @Suppress("DEPRECATION")
            val display = activity.windowManager.defaultDisplay
            @Suppress("DEPRECATION")
            display.getMetrics(outMetrics)
        }
        lp.height = outMetrics.heightPixels - Utils.convertDpToPixel(200, activity)
        lp.width = outMetrics.widthPixels - Utils.convertDpToPixel(30, activity)
//        lp.gravity = Gravity.BOTTOM
        dialog.window?.attributes = lp
        dialog.show()
        dialog.setOnKeyListener(object : DialogInterface.OnKeyListener{
            override fun onKey(p0: DialogInterface?, keycode: Int, p2: KeyEvent?): Boolean {
                if (keycode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss()
                }
                return true
            }
        })
    }

    private fun animationDialog(activity: Activity, valueUnlock: Int, strName: String) {
        val nameTemp = strName
        val dialog = Dialog(activity,R.style.DialogSlideUp)
        val lp = WindowManager.LayoutParams()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.cofirm_dialog, null)
        val noBtn = view.findViewById<AppCompatTextView>(R.id.tv_no)
        noBtn.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                dialog.dismiss()
            }
        })
        val yesBtn = view.findViewById<AppCompatTextView>(R.id.tv_yes)
        yesBtn.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                Utils.credits -= valueUnlock
                Utils.historyList.add(nameTemp)
                Utils.updateDataToFirebase()
//                val editor = mPrefs.edit();
//                editor.putInt(eulaKey, creditTemp);
//                editor.apply()
//                listener?.onUpdateCredit(creditTemp)
                listener?.onClick(nameTemp)
                dialog.dismiss()

            }
        })
        dialog.setContentView(view)
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.window?.attributes = lp
        dialog.show()
        dialog.setOnKeyListener(object : DialogInterface.OnKeyListener{
            override fun onKey(p0: DialogInterface?, keycode: Int, p2: KeyEvent?): Boolean {
                if (keycode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss()
                }
                return true
            }
        })
    }

    fun setOnListener(listener: VipListener) {
        this.listener = listener
    }

    fun setItemsUnlock(list: HashMap<String,String>) {
        mapItemUnLock.clear()
        mapItemUnLock.putAll(list)
    }

    fun setItemMatch(item: MatchFootball) {
        this.itemMatch = item
    }

    fun setIdMatch(id: String) {
        this.matchId = id
    }

    fun setData(list: ArrayList<Tips>) {
        tips.clear()
        tips.addAll(list)
        notifyDataSetChanged()
    }

}