package com.vn.footballtips.Screen.History

import android.content.Context
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.example.MatchFootball
import com.example.example.Tips
import com.example.example.clone
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vn.footballtips.R
import com.vn.footballtips.Screen.FreeTips.FreeAdapater
import com.vn.footballtips.Utils.Utils
import java.lang.reflect.Type
import java.util.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HistoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HistoryFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var recycleviewHistory: RecyclerView? = null
    private var historyAdapter: HistoryAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_history, container, false)
        initView(view)
        return view
    }

    private fun initView(view: View) {
        recycleviewHistory = view.findViewById(R.id.recycle_history)
        context?.let {
            historyAdapter = HistoryAdapter(it)
            val layoutManager = LinearLayoutManager(it, LinearLayoutManager.VERTICAL, false)
            recycleviewHistory?.layoutManager = layoutManager
            recycleviewHistory?.adapter = historyAdapter
            processData()
        }
    }

    fun processData() {
        val list = ArrayList<MatchFootball>()
        if (Utils.matchFootballs.isNotEmpty()) {
            for (item in Utils.matchFootballs) {
                val tips = item.tips
                for (tip in tips) {
                    val key = item.matchId.toString()?.plus(tip.id)
                    if (Utils.historyList.contains(key)) {
                        val itemFootball = item.clone()
                        val tipsTemp = ArrayList<Tips>()
                        tipsTemp.add(tip)
                        itemFootball.tips.clear()
                        itemFootball.tips.addAll(tipsTemp)
                        list.add(itemFootball)
                    }
                }
            }
        }
        historyAdapter?.setData(list)
    }

    fun getItemUnLock(context: Context?): HashMap<String, String> {
        val mapItems: HashMap<String, String>
        val mPrefs = context?.getSharedPreferences("FootballPrefs", Context.MODE_PRIVATE);
        val items =mPrefs?.getString("unLock", null)
        mapItems = if (items != null) {
            val gson = Gson()
            val type: Type = object : TypeToken<HashMap<String?, String?>?>() {}.type
            gson.fromJson(items, type)
        } else {
            HashMap()
        }
        return mapItems
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HistoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            HistoryFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}