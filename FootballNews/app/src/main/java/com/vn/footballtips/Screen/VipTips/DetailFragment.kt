package com.vn.footballtips.Screen.VipTips

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.example.MatchFootball
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.picasso.Picasso
import com.vn.footballtips.R
import com.vn.footballtips.Utils.Utils
import java.lang.reflect.Type

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
/**
 * A simple [Fragment] subclass.
 * Use the [DetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailFragment : Fragment() {
    // TODO: Rename and change types of parameters
    var timerView: AppCompatTextView? = null
    var leageView: AppCompatTextView? = null
    var awayView: AppCompatTextView? = null
    var homeView: AppCompatTextView? = null
    var rateView: AppCompatTextView? = null
    var scoreView: AppCompatTextView? = null
    var homeLogo: AppCompatImageView? = null
    var awayLogo: AppCompatImageView? = null
    private var recycleViewTips: RecyclerView? = null
    private var item: MatchFootball? = null
    private var adapter: DetailAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = this.arguments
        item = bundle?.getParcelable("itemMatch")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        initView(view)
        return view
    }

    @SuppressLint("SimpleDateFormat")
    private fun initView(view: View) {
        timerView = view.findViewById(R.id.tv_date_time)
        leageView = view.findViewById(R.id.tv_leage)
        awayView = view.findViewById(R.id.tv_away)
        homeView = view.findViewById(R.id.tv_home)
        rateView = view.findViewById(R.id.tv_rate)
        scoreView = view.findViewById(R.id.tv_score)
        homeLogo = view.findViewById(R.id.img_home)
        awayLogo = view.findViewById(R.id.img_away)
//        imgLock = view.findViewById(R.id.img_lock)
        recycleViewTips = view.findViewById(R.id.recycleview_tips)
        context?.let {
            val layoutManager = LinearLayoutManager(it, LinearLayoutManager.VERTICAL, false)
            recycleViewTips?.layoutManager = layoutManager
            val divider = Utils.SpacesItemDecoration(25);
            recycleViewTips?.addItemDecoration(divider)
            adapter = DetailAdapter(it)
            adapter?.setOnListener(listener)
            recycleViewTips?.adapter = adapter
//            adapter?.setItemsUnlock(mapItemsUnlock)

        }

        Picasso.get()
            .load(item?.homeFlag)
            .into(homeLogo)
        Picasso.get()
            .load(item?.awayFlag)
            .into(awayLogo)
        val time = item?.time?.toLong()

        if (time !=0L) {
            if (time != null) {
                val newDate = DateFormat.format("yyyy-MM-dd hh:mm",time * 1000)
                timerView?.text = newDate
            }
        }
        leageView?.text = item?.leage
        homeView?.text = item?.home
        awayView?.text = item?.away
        item?.let { adapter?.setItemMatch(it) }
        adapter?.setIdMatch(item?.matchId.toString())
        adapter?.setData(item?.tips?: ArrayList())

    }

    val listener: VipListener = object : VipListener {
        override fun onClick(value: String) {
            super.onClick(value)
//            addItemUnLock(context, value)
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed(Runnable {
//                mapItemsUnlock.clear()
//                mapItemsUnlock = getItemUnLock(context)
//                adapter?.setItemsUnlock(mapItemsUnlock)
                adapter?.notifyDataSetChanged()
            }, 400)
        }

        override fun onShowDetail(item: MatchFootball) {
            TODO("Not yet implemented")
        }
    }

    fun addItemUnLock(context: Context?, name: String) {
        val items: HashMap<String, String> = getItemUnLock(context)
        items[name] = name
        val gson = Gson()
        val hashMapString = gson.toJson(items)
        val mPrefs = context?.getSharedPreferences("FootballPrefs", Context.MODE_PRIVATE)
        val editor = mPrefs?.edit();
        editor?.putString("unLock", hashMapString);
        editor?.commit()
    }

    fun getItemUnLock(context: Context?): HashMap<String, String> {
        val mapItems: HashMap<String, String>
        val mPrefs = context?.getSharedPreferences("FootballPrefs", Context.MODE_PRIVATE);
        val items =mPrefs?.getString("unLock", null)
        mapItems = if (items != null) {
            val gson = Gson()
            val type: Type = object : TypeToken<HashMap<String?, String?>?>() {}.type
            gson.fromJson(items, type)
        } else {
            HashMap()
        }
        return mapItems
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DetailFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            DetailFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}