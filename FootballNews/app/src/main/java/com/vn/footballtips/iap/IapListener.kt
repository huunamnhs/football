package com.vn.footballtips.iap

import com.android.billingclient.api.ProductDetails

interface IapListener {
    open fun onClick(productDetails: ProductDetails) {}
}