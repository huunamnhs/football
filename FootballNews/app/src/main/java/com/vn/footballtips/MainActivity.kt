package com.vn.footballtips

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.example.MatchFootball
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.AuthResult
import com.vn.footballtips.Interface.MatchApi
import com.vn.footballtips.Network.ApiUtils
import com.vn.footballtips.Screen.Account.AccountFragment
import com.vn.footballtips.Screen.Account.LoginFragment
import com.vn.footballtips.Screen.Account.RegisterFragment
import com.vn.footballtips.Screen.FreeTips.FreeTipsFragment
import com.vn.footballtips.Screen.History.HistoryFragment
import com.vn.footballtips.Screen.VipTips.DetailFragment
import com.vn.footballtips.Screen.VipTips.VipTipsFragment
import com.vn.footballtips.Utils.Utils
import com.vn.footballtips.Utils.Utils.choiceDate
import com.vn.footballtips.iap.IapFragment
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {
    private var freeFragment: FreeTipsFragment? = null
    private var iapFragment: IapFragment? = null
    private var historyFragment: HistoryFragment? = null
    private var vipFragment: VipTipsFragment? = null
    private var accountFragment: LoginFragment? = null
    private var registerFragment: RegisterFragment? = null
    private var detailFragment: DetailFragment? = null
    private val mParentJob = SupervisorJob()
    private var loadingBar: ProgressBar? = null
    private var pullToRefresh: SwipeRefreshLayout? = null
    private var currentFragment: Fragment? = null
    private var listFragment = ArrayList<Fragment>()
    private var toolbar: Toolbar? = null
    private var tvCredit: AppCompatTextView? = null
    private var tvCalendar: AppCompatTextView?= null
    private var topView: View?= null
    private var isLoading = false
    var isBack = false

    val mCoroutineScope = CoroutineScope(mParentJob + Dispatchers.Main)
    private val handler = CoroutineExceptionHandler { context, throwable ->
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        loadData()
    }

    private fun initView() {
        loadingBar = findViewById(R.id.progressBar)
        initBottomNavigationView()
        toolbar = findViewById(R.id.toolbarView)
        topView = findViewById(R.id.top_view)
        tvCredit = findViewById(R.id.tv_credit)
        topView?.visibility = View.GONE
        tvCredit?.visibility = View.GONE
        setCredit()
        getSupportActionBar()?.hide()
        setSupportActionBar(toolbar)

        pullToRefresh = findViewById<SwipeRefreshLayout>(R.id.pullToRefresh)
        pullToRefresh?.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                reLoadData()
            }
        })
        initCalendar()
    }

    override fun onResume() {
        super.onResume()
        Utils.onDetectLogin(this)
    }

    fun setCredit() {
        tvCredit?.text = "Credtit: ".plus(Utils.credits)
    }

    private fun initCalendar() {
        val myCalendar = Calendar.getInstance()
        tvCalendar = findViewById(R.id.tv_calendar)
        tvCalendar?.visibility = View.GONE
        val myFormat = "dd MMMM yyyy" // your format
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        tvCalendar?.text = sdf.format(myCalendar.time)
        choiceDate = sdf.format(myCalendar.time)
        tvCalendar?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(view: View?) {
                val date =
                    OnDateSetListener { view, year, monthOfYear, dayOfMonth -> // TODO Auto-generated method stub
                        myCalendar[Calendar.YEAR] = year
                        myCalendar[Calendar.MONTH] = monthOfYear
                        myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                        val myFormat = "dd MMMM yyyy" // your format
                        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
                        tvCalendar?.text = "<<  " + (sdf.format(myCalendar.time)) + "  >>"
                        choiceDate = sdf.format(myCalendar.time)
                        if (currentFragment is VipTipsFragment) {
                            (currentFragment as VipTipsFragment).processData()
                        } else if (currentFragment is FreeTipsFragment) {
                            (currentFragment as FreeTipsFragment).processData()
                        }
                    }
                DatePickerDialog(
                    this@MainActivity,
                    date,
                    myCalendar[Calendar.YEAR],
                    myCalendar[Calendar.MONTH],
                    myCalendar[Calendar.DAY_OF_MONTH]
                ).show()

            }
        })
    }

    private fun hideShowCalendar(isShow: Boolean) {
        tvCalendar?.visibility = if (isShow) View.VISIBLE else View.GONE
    }
    private fun setToolBarTitle(fragment: Fragment) {
        var title = "Free"
        if (fragment is VipTipsFragment) {
            title = "Vips"
        } else if (fragment is IapFragment) {
            title = "Shop"
        }else if (fragment is HistoryFragment) {
            title = "History"
        } else if (fragment is AccountFragment || fragment is LoginFragment || fragment is RegisterFragment) {
            title = "Account"
        }
        toolbar?.findViewById<AppCompatTextView>(R.id.title_toolbar)?.text = title// 2eca69 eaeaea
    }

    private fun loadData() {
        isLoading = true
        val matchApi: MatchApi = ApiUtils().getMatchService()!!
        mCoroutineScope.launch(handler) {
            val job = async(Dispatchers.IO) {
                try {
                    Utils.matchFootballs = matchApi.getMatchFootball()?.body()?: ArrayList()
                } catch (e: Exception) {
                    Utils.matchFootballs = ArrayList()
                }
            }
            job.join()
            freeFragment = FreeTipsFragment.newInstance()
            freeFragment?.let {
                setCurrentFragment(it)
                currentFragment = it
            }
            loadingBar?.visibility = View.GONE
            pullToRefresh?.setRefreshing(false);
            tvCalendar?.visibility = View.VISIBLE
            tvCredit?.visibility = View.VISIBLE
            topView?.visibility = View.VISIBLE
            isLoading = false
        }
    }

    private fun reLoadData() {
        val matchApi: MatchApi = ApiUtils().getMatchService()!!
        mCoroutineScope.launch(handler) {
            val job = async(Dispatchers.IO) {
                try {
                    Utils.matchFootballs = matchApi.getMatchFootball()?.body()?: ArrayList()
                } catch (e: Exception) {
                    Utils.matchFootballs = ArrayList()
                }
            }
            job.await()
            if (currentFragment is FreeTipsFragment) {
                setCurrentFragment(currentFragment as FreeTipsFragment)
            } else if (currentFragment is VipTipsFragment) {
                setCurrentFragment(currentFragment as VipTipsFragment)
            }
            loadingBar?.visibility = View.GONE
            pullToRefresh?.setRefreshing(false);
        }
    }

    private fun initBottomNavigationView() {
        val bottomNavigationView = findViewById(R.id.bottomNavigationView) as BottomNavigationView
        bottomNavigationView.setOnItemSelectedListener {
            if (isLoading) true
            if (listFragment.isNotEmpty()) {
                val fragment = supportFragmentManager.findFragmentById(R.id.container_fragment)
                if (fragment?.equals(listFragment.get(listFragment.size - 1)) == true && isBack) {
                    setToolBarTitle(fragment)
                    true
                } else {
                    when(it.itemId){
                        R.id.free_tips-> {
                            if (freeFragment == null) {
                                freeFragment = FreeTipsFragment.newInstance()
                            }
                            freeFragment?.let {
                                setCurrentFragment(it)
                            }
                        }
                        R.id.vip_tips-> {
                            if (vipFragment == null) {
                                vipFragment = VipTipsFragment.newInstance()
                            }
                            vipFragment?.let {
                                setCurrentFragment(it)
                            }
                        }
                        R.id.static_tips -> {
                            if (iapFragment == null) {
                                iapFragment = IapFragment.newInstance()
                            }
                            iapFragment?.let {
                                setCurrentFragment(it)
                            }
                        }
                        R.id.history -> {
                            if (historyFragment == null) {
                                historyFragment = HistoryFragment.newInstance()
                            }
                            historyFragment?.let {
                                setCurrentFragment(it)
                            }
                        }
                        R.id.account_tips -> {

                            if (accountFragment == null) {
                                accountFragment = LoginFragment.newInstance()
                            }
                            accountFragment?.let {
                                setCurrentFragment(it)
                            }
                        }
                    }
                }
            }


            isBack = false
            true
        }
    }

    fun  openRegisterScreen() {
        if (registerFragment == null) {
            registerFragment = RegisterFragment.newInstance()
        }
        registerFragment?.let {
            setCurrentFragment(it)
        }
    }

    fun openIAP() {
        if (iapFragment == null) {
            iapFragment = IapFragment.newInstance()
        }
        iapFragment?.let {
            setCurrentFragment(it)
        }
    }

    private fun setCurrentFragment(fragment: Fragment)=
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.container_fragment,fragment)
            addToBackStack(null)
            commit()
            currentFragment = fragment
            setToolBarTitle(fragment)
            listFragment.add(fragment)
            hideShowCalendar(true)
            if (fragment is IapFragment || fragment is LoginFragment || fragment is HistoryFragment || fragment is RegisterFragment) {
                hideShowCalendar(false)
            }
        }

    override fun onBackPressed() {
        val fragmentOld = supportFragmentManager.findFragmentById(R.id.container_fragment)
        super.onBackPressed()
        if (listFragment.isNotEmpty()) {
            listFragment.removeAt(listFragment.size - 1)
        }
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed(Runnable {
            val fragment = supportFragmentManager.findFragmentById(R.id.container_fragment)
            currentFragment = fragment
            isBack = true
            val bottomNavigationView = findViewById(R.id.bottomNavigationView) as BottomNavigationView

            if (fragment is FreeTipsFragment) {
                bottomNavigationView.selectedItemId = R.id.free_tips
            } else if (fragment is VipTipsFragment) {
                bottomNavigationView.selectedItemId = R.id.vip_tips
            } else if (fragment is IapFragment) {
                bottomNavigationView.selectedItemId = R.id.static_tips
            } else if (fragment is LoginFragment) {
                bottomNavigationView.selectedItemId = R.id.account_tips
            } else if (fragment is HistoryFragment) {
                bottomNavigationView.selectedItemId = R.id.history
            }
            fragmentOld?.let {
                if (it is FreeTipsFragment) {
                    finish()
                }
            }
        }, 400)
    }

    fun openDetail(item: MatchFootball) {
        if (detailFragment == null) {
            detailFragment = DetailFragment.newInstance()
        }
        detailFragment?.let {
            val bundle =  Bundle();
            bundle.putParcelable("itemMatch", item)  // Key, value
            detailFragment?.setArguments(bundle);
            setCurrentFragment(it)
        }

    }

    fun openAccountScreen() {
        val bottomNavigationView = findViewById(R.id.bottomNavigationView) as BottomNavigationView
        bottomNavigationView.selectedItemId = R.id.account_tips
    }
}