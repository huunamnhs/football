package com.vn.footballtips.iap

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.billingclient.api.*
import com.vn.footballtips.MainActivity
import com.vn.footballtips.R
import com.vn.footballtips.Utils.Utils

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Use the [IapFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class IapFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var billingClient: BillingClient? = null
    private var recyclerViewIAP: RecyclerView? = null
    private var iapAdapter: IapAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_iap, container, false)
        initView(view)
        billingClient = context?.let {
            BillingClient.newBuilder(it)
                .setListener(purchasesUpdatedListener)
                .enablePendingPurchases()
                .build()
        }
        connectToGooglePlay()
        return view
    }

    private fun initView(view: View) {
        recyclerViewIAP = view.findViewById(R.id.recycleview_iap)
        iapAdapter = context?.let { IapAdapter(it) }
        iapAdapter?.setListener(listener = iapListener)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerViewIAP?.layoutManager = layoutManager
        val divider = Utils.SpacesItemDecoration(25);
        recyclerViewIAP?.addItemDecoration(divider)
        recyclerViewIAP?.adapter = iapAdapter
    }

    private val purchasesUpdatedListener =
        PurchasesUpdatedListener { billingResult, purchases ->
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
                for (purchase in purchases) {
                    handlePurchase(purchase)
                }
            }
        }

    private fun handlePurchase(purchase: Purchase) {
        if (purchase.products.isNotEmpty()) {
            when(purchase.products.get(0)) {
                Utils.IAP_ITEM_1 -> {
                    Utils.credits += 100
                    Utils.updateDataToFirebase()
                }
                Utils.IAP_ITEM_2 -> {
                    Utils.credits += 1000
                    Utils.updateDataToFirebase()
                }
                Utils.IAP_ITEM_3 -> {
                    Utils.credits += 2000
                    Utils.updateDataToFirebase()
                }
                Utils.IAP_ITEM_4 -> {
                    Utils.credits += 3000
                    Utils.updateDataToFirebase()
                }
                Utils.IAP_ITEM_5 -> {
                    Utils.credits += 4000
                    Utils.updateDataToFirebase()
                }
            }
        }
        val consumeParams = ConsumeParams.newBuilder().setPurchaseToken(purchase.purchaseToken).build()
        billingClient?.consumeAsync(consumeParams, object : ConsumeResponseListener {
            override fun onConsumeResponse(p0: BillingResult, p1: String) {
                Log.i("APP","consumeAsync")
            }
        })
    }

    private fun connectToGooglePlay() {
        billingClient?.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode ==  BillingClient.BillingResponseCode.OK) {
                    getProductDetails()
                }
            }
            override fun onBillingServiceDisconnected() {
                connectToGooglePlay()
            }
        })
    }

    private fun getProductDetails() {
        val listItem = listOf(
            QueryProductDetailsParams.Product.newBuilder()
                .setProductId("football_item_1")
                .setProductType(BillingClient.ProductType.INAPP)
                .build(),
            QueryProductDetailsParams.Product.newBuilder()
                .setProductId("football_item_2")
                .setProductType(BillingClient.ProductType.INAPP)
                .build(),
            QueryProductDetailsParams.Product.newBuilder()
                .setProductId("football_item_3")
                .setProductType(BillingClient.ProductType.INAPP)
                .build(),
            QueryProductDetailsParams.Product.newBuilder()
                .setProductId("football_item_4")
                .setProductType(BillingClient.ProductType.INAPP)
                .build(),
            QueryProductDetailsParams.Product.newBuilder()
                .setProductId("football_item_5")
                .setProductType(BillingClient.ProductType.INAPP)
                .build())
        val queryProductDetailsParams =
            QueryProductDetailsParams.newBuilder()
                .setProductList(
                    listItem)
                .build()

        billingClient?.queryProductDetailsAsync(queryProductDetailsParams) {
                billingResult,
                productDetailsList ->
            if (billingResult?.responseCode == BillingClient.BillingResponseCode.OK && productDetailsList!= null) {
                val listItem = ArrayList<ProductDetails>()
                for (item in productDetailsList) {
                    listItem.add(item)
                }
                activity?.runOnUiThread(java.lang.Runnable {
                    iapAdapter?.setData(listItem)
                })
            }
        }
    }

    private val iapListener: IapListener = object : IapListener {
        override fun onClick(productDetails: ProductDetails) {
            val productDetailsParamsList = listOf(
                BillingFlowParams.ProductDetailsParams.newBuilder()
                    .setProductDetails(productDetails)
                    .build()
            )

            val billingFlowParams = BillingFlowParams.newBuilder()
                .setProductDetailsParamsList(productDetailsParamsList)
                .build()

            val billingResult =
                activity?.let { billingClient?.launchBillingFlow(it, billingFlowParams) }
        }
    }

    override fun onDestroyView() {
        billingClient?.endConnection()
        billingClient = null
        super.onDestroyView()
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment IapFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            IapFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}