package com.vn.footballtips.Screen.VipTips

import com.example.example.MatchFootball

interface VipListener {
    fun onClick(value: String){}
    fun onUpdateCredit(credit: Int){}
    fun onShowDetail(item: MatchFootball)
}