package com.vn.footballtips.Screen.VipTips

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.example.MatchFootball
import com.example.example.Tips
import com.squareup.picasso.Picasso
import com.vn.footballtips.R
import com.vn.footballtips.Utils.Utils
import java.text.SimpleDateFormat


class VipAdapater(val context: Context): RecyclerView.Adapter<VipAdapater.ViewHolder>() {
    private var items = ArrayList<MatchFootball>()
    private var listener: VipListener? = null
    private var mapItemUnLock = HashMap<String, String>()
    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvCompleteStatus: AppCompatTextView? = null
        var timerView: AppCompatTextView? = null
        var leageView: AppCompatTextView? = null
        var awayView: AppCompatTextView? = null
        var homeView: AppCompatTextView? = null
        var rateView: AppCompatTextView? = null
        var scoreViewHome: AppCompatTextView? = null
        var scoreViewAway: AppCompatTextView? = null
        var homeLogo: AppCompatImageView? = null
        var awayLogo: AppCompatImageView? = null
        var imgLock: AppCompatImageView? = null
        init {
            tvCompleteStatus = itemView.findViewById(R.id.tv_complete_status)
            timerView = itemView.findViewById(R.id.tv_date_time)
            leageView = itemView.findViewById(R.id.tv_leage)
            awayView = itemView.findViewById(R.id.tv_away)
            homeView = itemView.findViewById(R.id.tv_home)
            rateView = itemView.findViewById(R.id.tv_rate)
            scoreViewHome = itemView.findViewById(R.id.tv_score_home)
            scoreViewAway = itemView.findViewById(R.id.tv_score_away)
            homeLogo = itemView.findViewById(R.id.img_home)
            awayLogo = itemView.findViewById(R.id.img_away)
            imgLock = itemView.findViewById(R.id.img_lock)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_vip_1, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items.get(position)
        item.let {
            Picasso.get()
                .load(it.homeFlag)
                .into(holder.homeLogo)
            Picasso.get()
                .load(it.awayFlag)
                .into(holder.awayLogo)
            val time = item.time?.toLong()
            if (time !=0L) {
                if (time != null) {
                    val newDate = DateFormat.format("hh:mm",time * 1000)
                    holder.timerView?.text = newDate
                    Log.i("NAMTH","newDate=${newDate}::=${item.finalScore}::=${item.status}")
                }
            }
            holder.leageView?.text = item.leage
            holder.homeView?.text = item.home
            holder.awayView?.text = item.away
            if (item.status == 0) {
                holder.scoreViewAway?.visibility = View.GONE
                holder.scoreViewHome?.visibility = View.GONE
                holder.tvCompleteStatus?.visibility = View.VISIBLE
                holder.tvCompleteStatus?.text = "RAW"
            } else {
                holder.scoreViewHome?.visibility = View.VISIBLE
                Log.i("NAMTH","item.finalScore=${item.finalScore}")
                Log.i("NAMTH","item1=${item.finalScore?.substring(0, item.finalScore!!.indexOf("-"))?.trim()}")
                Log.i("NAMTH","item2=${item.finalScore?.length?.let { it1 ->
                    item.finalScore?.substring(item.finalScore!!.indexOf("-") + 1,
                        it1
                    )?.trim()}}")
                holder.scoreViewHome?.text = item.finalScore?.substring(0, item.finalScore!!.indexOf("-"))?.trim()
                holder.scoreViewAway?.visibility = View.VISIBLE
                holder.scoreViewAway?.text = item.finalScore?.length?.let { it1 ->
                    item.finalScore?.substring(item.finalScore!!.indexOf("-") + 1,
                        it1
                    )?.trim()
                }
                holder.tvCompleteStatus?.visibility = View.GONE

            }
            var index = 0
            var maxRate = 0.0
            val tips = ArrayList<Tips>()
            if (item.tips.isNotEmpty()) {
                for (i in 0..item.tips.size-1) {
                    val tip = item.tips[i]
                    val unlock = item.matchId.toString().plus(tip.id)
                    if (Utils.historyList.contains(unlock)) {
                        tips.add(tip)
                    }

                }
            }
            if (tips.isNotEmpty()) {
                for (i in 0..tips.size-1) {
                    val tip = item.tips[i]
                    if (maxRate < tip.rate ?: 0.0) {
                        maxRate = tip.rate ?: 0.0
                        index = i
                    }
                }
            }
            val tip = item.tips.get(index)
            tip.let {
                val unlock = item.matchId.toString().plus(tip.id)
                if (it.coinfee != 0 && Utils.historyList.contains(unlock).not()) {
                    holder.imgLock?.visibility = View.VISIBLE
                    holder.rateView?.visibility = View.GONE

                } else {
                    holder.imgLock?.visibility = View.GONE
                    holder.rateView?.visibility = View.VISIBLE
                    holder.rateView?.text = tip.rate.toString()

                }
                holder.itemView.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(p0: View?) {
                        listener?.onShowDetail(item)
                    }

                })
            }

            holder.itemView.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(p0: View?) {
                        listener?.onShowDetail(item)
                    }
                })
        }
    }


    private fun showConfirmDialog(valueUnlock: Int, credit: Int, strName: String) {
//        var creditTemp = credit
//        val nameTemp = strName
//        val alertDialog: AlertDialog? = context.let {
//            val builder = AlertDialog.Builder(it)
//            builder.apply {
//                setMessage("Are you sure want to buy this is item")
//                setCancelable(false)
//                setPositiveButton("OK",
//                    DialogInterface.OnClickListener { dialog, id ->
//                        creditTemp -= valueUnlock
//                        val editor = mPrefs.edit();
//                        editor.putInt(eulaKey, creditTemp);
//                        editor.apply()
//                        listener?.onUpdateCredit(creditTemp)
//                        listener?.onClick(nameTemp)
//
//                    })
//                setNegativeButton("CANCEL",
//                    DialogInterface.OnClickListener { dialog, id ->
//                    })
//            }
//            // Set other dialog properties
//            builder.create()
//        }
//        alertDialog?.show()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setOnListener(listener: VipListener) {
        this.listener = listener
    }

    fun setItemsUnlock(list: HashMap<String,String>) {
        mapItemUnLock.clear()
        mapItemUnLock.putAll(list)
    }

    fun setData(list: ArrayList<MatchFootball>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }
}