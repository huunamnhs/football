package com.vn.footballtips.Screen.History

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.text.format.DateFormat
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.example.MatchFootball
import com.squareup.picasso.Picasso
import com.vn.footballtips.MainActivity
import com.vn.footballtips.R
import com.vn.footballtips.Screen.FreeTips.FreeAdapater
import com.vn.footballtips.Utils.Utils
import java.util.ArrayList

class HistoryAdapter(val context: Context): RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {
    private var items = ArrayList<MatchFootball>()
    private var mapItemUnLock = HashMap<String, String>()

    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var timerView: AppCompatTextView? = null
        var leageView: AppCompatTextView? = null
        var awayView: AppCompatTextView? = null
        var homeView: AppCompatTextView? = null
        var rateView: AppCompatTextView? = null
        var scoreView: AppCompatTextView? = null
        var tvTipName: AppCompatTextView? = null
        var tvSelection: AppCompatTextView? = null
        var tvAccuracy: AppCompatTextView? = null
        var tvCompleteStatus: AppCompatTextView? = null
        var homeLogo: AppCompatImageView? = null
        var awayLogo: AppCompatImageView? = null

        init {
            timerView = itemView.findViewById(R.id.tv_date_time)
            leageView = itemView.findViewById(R.id.tv_leage)
            awayView = itemView.findViewById(R.id.tv_away)
            homeView = itemView.findViewById(R.id.tv_home)
            rateView = itemView.findViewById(R.id.tv_rate)
            scoreView = itemView.findViewById(R.id.tv_score)
            homeLogo = itemView.findViewById(R.id.img_home)
            awayLogo = itemView.findViewById(R.id.img_away)
            tvTipName = itemView.findViewById(R.id.tv_tip_name)
            tvSelection = itemView.findViewById(R.id.tv_selection)
            tvAccuracy = itemView.findViewById(R.id.tv_accuracy)
            tvCompleteStatus = itemView.findViewById(R.id.tv_complete_status)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_history, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items.get(position)
        item.let {
            Picasso.get()
                .load(it.homeFlag)
                .into(holder.homeLogo)
            Picasso.get()
                .load(it.awayFlag)
                .into(holder.awayLogo)
            val time = item.time?.toLong()

            if (time !=0L) {
                if (time != null) {
                    val newDate = DateFormat.format("yyyy-MM-dd hh:mm",time * 1000)
                    holder.timerView?.text = newDate
                }
            }
            holder.leageView?.text = item.leage
            holder.homeView?.text = item.home
            holder.awayView?.text = item.away
            val tip = item.tips[0]
            if (tip.status == 1) {
                holder.tvCompleteStatus?.text = "WIN"
                holder.tvCompleteStatus?.setTextColor(ContextCompat.getColor(context, R.color.win))
            } else if (tip.status == 3) {
                holder.tvCompleteStatus?.text = "LOSE"
                holder.tvCompleteStatus?.setTextColor(ContextCompat.getColor(context, R.color.lose))
            }
            holder.tvTipName?.text = tip.name
            holder.tvSelection?.text = tip.selection
            holder.tvAccuracy?.text = tip.accuracy.toString().plus("%")
            holder.rateView?.text = tip.rate.toString()
            holder.scoreView?.text = it.finalScore
            holder.scoreView?.visibility = if (tip.status != 0) View.VISIBLE else View.GONE
            holder.itemView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(view: View?) {
//                    val item = items.get(holder.adapterPosition)
//                    ViewAllDialog(activity = (context as MainActivity), item)
                }
            })
        }
    }

    private fun ViewAllDialog(activity: Activity, item: MatchFootball) {
        val dialog = Dialog(activity, R.style.DialogSlideUp)
        val lp = WindowManager.LayoutParams()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_free1, null)

        val tvLeage = view.findViewById<AppCompatTextView>(R.id.tv_leage)
        val tvDateTime = view.findViewById<AppCompatTextView>(R.id.tv_date_time)
        val tvScore = view.findViewById<AppCompatTextView>(R.id.tv_score)
        val tvRate = view.findViewById<AppCompatTextView>(R.id.tv_rate)
        val tvHome = view.findViewById<AppCompatTextView>(R.id.tv_home)
        val tvAway = view.findViewById<AppCompatTextView>(R.id.tv_away)
        val tvTipName = view.findViewById<AppCompatTextView>(R.id.tv_tip_name)
        val tvSelection = view.findViewById<AppCompatTextView>(R.id.tv_selection)
        val tvAccuracy = view.findViewById<AppCompatTextView>(R.id.tv_accuracy)
        val tvOpinion = view.findViewById<AppCompatTextView>(R.id.tv_opinion)
        val imgHome = view.findViewById<AppCompatImageView>(R.id.img_home)
        val imgAway = view.findViewById<AppCompatImageView>(R.id.img_away)
        val imgBack = view.findViewById<AppCompatImageView>(R.id.img_back)

        tvLeage.text = item.leage
        tvDateTime.text = item.time
        tvScore.text = item.finalScore
        tvHome.text = item.home
        tvAway.text = item.away
        Picasso.get()
            .load(item.homeFlag)
            .into(imgHome)
        Picasso.get()
            .load(item.awayFlag)
            .into(imgAway)
        val tip = item.tips.get(0)
        tip?.let {
            tvRate.text = it.rate.toString()
            tvTipName.text = it.name
            tvSelection.text = it.selection
            tvAccuracy.text = it.accuracy.toString().plus("%")
            tvOpinion.text = it.expertOpinion
        }
        imgBack.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                dialog.dismiss()
            }
        })
        dialog.setContentView(view)
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        val outMetrics = DisplayMetrics()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            val display = activity.display
            display?.getRealMetrics(outMetrics)
        } else {
            @Suppress("DEPRECATION")
            val display = activity.windowManager.defaultDisplay
            @Suppress("DEPRECATION")
            display.getMetrics(outMetrics)
        }
        lp.height = outMetrics.heightPixels - Utils.convertDpToPixel(200, activity)
        lp.width = outMetrics.widthPixels - Utils.convertDpToPixel(30, activity)
        dialog.window?.attributes = lp
        dialog.show()
        dialog.setOnKeyListener(object : DialogInterface.OnKeyListener{
            override fun onKey(p0: DialogInterface?, keycode: Int, p2: KeyEvent?): Boolean {
                if (keycode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss()
                }
                return true
            }
        })
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItemsUnlock(list: HashMap<String,String>) {
        mapItemUnLock.clear()
        mapItemUnLock.putAll(list)
    }

    fun setData(list: ArrayList<MatchFootball>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }
}