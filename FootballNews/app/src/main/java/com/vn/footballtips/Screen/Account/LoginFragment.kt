package com.vn.footballtips.Screen.Account

import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.vn.footballtips.MainActivity
import com.vn.footballtips.R
import com.vn.footballtips.Utils.Utils


/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment() {
    var nickname:AppCompatTextView? = null
    var tvCredit:AppCompatTextView? = null
    var tvLogout:AppCompatTextView? = null
    var username:AppCompatEditText? = null
    var password:AppCompatEditText? = null
    var usernameWarnning:AppCompatTextView? = null
    var passwordWarnning:AppCompatTextView? = null
    var tvLogin:AppCompatTextView? = null
    var tvRegister:AppCompatTextView? = null
    var imgUsernameWarning:AppCompatImageView? = null
    var imgPasswordWarning:AppCompatImageView? = null
    var loginLayout: View? = null
    var accountLayout: View? = null
    val auth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val coin = "coin"
    val history_item = "history_item"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        initView(view)
        return view
    }

    private fun initView(view: View) {
        nickname = view.findViewById(R.id.tv_nickname)
        tvCredit = view.findViewById(R.id.tv_credit)
        tvLogout = view.findViewById(R.id.tv_logout)
        username = view.findViewById<AppCompatEditText>(R.id.edit_username)
        password = view.findViewById<AppCompatEditText>(R.id.edit_password)
        usernameWarnning = view.findViewById<AppCompatTextView>(R.id.tv_warnning_username)
        passwordWarnning = view.findViewById<AppCompatTextView>(R.id.tv_warnning_password)
        tvLogin = view.findViewById<AppCompatTextView>(R.id.tv_login)
        tvRegister = view.findViewById<AppCompatTextView>(R.id.tv_register)
        imgUsernameWarning = view.findViewById<AppCompatImageView>(R.id.warning_username)
        imgPasswordWarning = view.findViewById<AppCompatImageView>(R.id.warning_password)
        loginLayout = view.findViewById(R.id.login_layout)
        accountLayout = view.findViewById(R.id.account_layout)

        usernameWarnning?.visibility = View.GONE
        passwordWarnning?.visibility = View.GONE
        imgUsernameWarning?.visibility = View.GONE
        imgPasswordWarning?.visibility = View.GONE
        password?.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        username?.setOnFocusChangeListener(OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                username?.setHint("")
                hideWarning()
            } else {
                username?.setHint("Email")
            }
        })
        password?.setOnFocusChangeListener(OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                password?.setHint("")
                hideWarning()
                password?.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
            } else {
                password?.setHint("Password")
            }
        })
        if (auth.currentUser != null) {
            nickname?.text = auth.currentUser?.email
            tvCredit?.text = "0"
            accountLayout?.visibility = VISIBLE
            loginLayout?.visibility = GONE
            getAccountInfo()
        } else {
            accountLayout?.visibility = GONE
            loginLayout?.visibility = VISIBLE
        }
        tvLogin?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                login()
            }
        })
        tvRegister?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(view: View?) {
                (context as MainActivity).openRegisterScreen()
            }
        })

        tvLogout?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(view: View?) {
                auth.signOut()
                loginLayout?.visibility = VISIBLE
                accountLayout?.visibility = GONE
                Utils.credits = 0
                Utils.historyList = ArrayList()
                (context as MainActivity).setCredit()
                tvCredit?.text = Utils.credits.toString().plus(" coin")
            }
        })
    }

    private fun getAccountInfo() {
        val docRef = auth.currentUser?.uid?.let { db.collection("user").document(it) }

        docRef?.addSnapshotListener { document, error ->
            if (error != null) {
                return@addSnapshotListener
            }
            if (document != null && document.data != null) {
                Utils.credits = document.data?.get(coin).toString().toInt()
                var history = document.data?.get(Utils.historyItemParam).toString()
                history = history.replace("\\s".toRegex(), "")
                if (history.equals("").not())
                    Utils.historyList = ArrayList(ArrayList(history.split(",").toList()))
            } else {
                Utils.credits = 0
                Utils.historyList = ArrayList()
                val docData = hashMapOf(coin to Utils.credits, history_item to "")
                docRef.set(docData)
            }
            (context as MainActivity).setCredit()
            tvCredit?.text = Utils.credits.toString().plus(" coin")

        }
    }

    private fun login() {
        val usernameInput = username?.text?.toString()?.trim()?:""
        val passwordInput = password?.text?.toString()?.trim()?:""
        if (usernameInput.equals("") == true) {
            usernameWarnning?.visibility = VISIBLE
            imgUsernameWarning?.visibility = VISIBLE
        } else if (passwordInput.equals("") == true) {
            passwordWarnning?.visibility = VISIBLE
            imgPasswordWarning?.visibility = VISIBLE
        } else {
            hideWarning()
            val auth = FirebaseAuth.getInstance()
//            auth.signInWithEmailAndPassword("huunamnhs@gmail.com", "123456")
            auth.signInWithEmailAndPassword(usernameInput, passwordInput)
                .addOnCompleteListener(object : OnCompleteListener<AuthResult> {
                    override fun onComplete(task: Task<AuthResult>) {
                        if (task.isSuccessful) {
                            Toast.makeText(context, "Login success", Toast.LENGTH_LONG).show()
                            loginLayout?.visibility = GONE
                            accountLayout?.visibility = VISIBLE
                            getAccountInfo()
                            nickname?.text = auth.currentUser?.email
                        } else {
                            Toast.makeText(context, "Login Fail", Toast.LENGTH_LONG).show()
                            loginLayout?.visibility = VISIBLE
                            accountLayout?.visibility = GONE
                        }
                    }
                })
        }
    }

    private fun hideWarning() {
        usernameWarnning?.visibility = View.GONE
        imgUsernameWarning?.visibility = View.GONE
        passwordWarnning?.visibility = View.GONE
        imgPasswordWarning?.visibility = View.GONE
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LoginFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            LoginFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}