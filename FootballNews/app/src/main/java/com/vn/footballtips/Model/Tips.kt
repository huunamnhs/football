package com.example.example

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tips (

  @SerializedName("id"           ) var id          : Int? = null,
  @SerializedName("name"           ) var name          : String? = null,
  @SerializedName("selection"      ) var selection     : String? = null,
  @SerializedName("rate"           ) var rate          : Double? = null,
  @SerializedName("symbol"         ) var symbol        : String? = null,
  @SerializedName("accuracy"       ) var accuracy      : Int?    = null,
  @SerializedName("coinfee"        ) var coinfee      : Int?    = null,
  @SerializedName("expert_opinion" ) var expertOpinion : String? = null,
  @SerializedName("status"         ) var status        : Int?    = null

) : Parcelable