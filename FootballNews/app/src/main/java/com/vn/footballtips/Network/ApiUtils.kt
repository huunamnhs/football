package com.vn.footballtips.Network

import com.vn.footballtips.Interface.MatchApi

class ApiUtils {
    val BASE_URL = "https://smartvpn.live/api/"
    fun getMatchService(): MatchApi? {
        return RetrofitHelper.getClient(BASE_URL)?.create(MatchApi::class.java)
    }
}