package com.vn.footballtips.Screen.Account

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.InputType
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.vn.footballtips.MainActivity
import com.vn.footballtips.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Use the [RegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RegisterFragment : Fragment() {
    // TODO: Rename and change types of parameters
    var username: AppCompatEditText? = null
    var password: AppCompatEditText? = null
    var confirmPassword: AppCompatEditText? = null
    var usernameWarnning: AppCompatTextView? = null
    var passwordWarnning: AppCompatTextView? = null
    var confirmPasswordWarnning: AppCompatTextView? = null
    var tvRegister: AppCompatTextView? = null
    var imgUsernameWarning:AppCompatImageView? = null
    var imgPasswordWarning:AppCompatImageView? = null
    var imgConfirmPasswordWarning:AppCompatImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_register, container, false)
        initView(view)
        return view
    }

    private fun initView(view: View) {
        username = view.findViewById<AppCompatEditText>(R.id.edit_username)
        password = view.findViewById<AppCompatEditText>(R.id.edit_password)
        confirmPassword = view.findViewById<AppCompatEditText>(R.id.edit_confirm_password)
        usernameWarnning = view.findViewById<AppCompatTextView>(R.id.tv_warnning_username)
        passwordWarnning = view.findViewById<AppCompatTextView>(R.id.tv_warnning_password)
        confirmPasswordWarnning = view.findViewById<AppCompatTextView>(R.id.tv_warnning_confirm_password)
        tvRegister = view.findViewById<AppCompatTextView>(R.id.tv_register)
        imgUsernameWarning = view.findViewById<AppCompatImageView>(R.id.warning_username)
        imgPasswordWarning = view.findViewById<AppCompatImageView>(R.id.warning_password)
        imgConfirmPasswordWarning = view.findViewById<AppCompatImageView>(R.id.warning_confirm_password)

        usernameWarnning?.visibility = View.GONE
        passwordWarnning?.visibility = View.GONE
        confirmPasswordWarnning?.visibility = View.GONE
        imgUsernameWarning?.visibility = View.GONE
        imgPasswordWarning?.visibility = View.GONE
        imgConfirmPasswordWarning?.visibility = View.GONE
        username?.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                username?.setHint("")
                hideAllWarning()
            } else {
                username?.setHint("Email")
            }
        })

        password?.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                password?.setHint("")
                hideAllWarning()
                password?.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
            } else {
                password?.setHint("Password")
            }
        })
        confirmPassword?.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                confirmPassword?.setHint("")
                hideAllWarning()
                confirmPassword?.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
            } else {
                confirmPassword?.setHint("Confirm Password")
            }
        })

        tvRegister?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(view: View?) {
                register()
            }
        })
    }

    private fun hideAllWarning() {
        usernameWarnning?.visibility = View.GONE
        imgUsernameWarning?.visibility = View.GONE
        passwordWarnning?.visibility = View.GONE
        imgPasswordWarning?.visibility = View.GONE
        confirmPasswordWarnning?.visibility = View.GONE
        imgConfirmPasswordWarning?.visibility = View.GONE
    }

    private fun register() {
        val usernameInput = username?.text?.toString()?.trim()?:""
        val passwordInput = password?.text?.toString()?.trim()?:""
        val confirmPasswordInput = confirmPassword?.text?.toString()?.trim()?:""
        if (usernameInput.equals("") == true) {
            usernameWarnning?.visibility = View.VISIBLE
            imgUsernameWarning?.visibility = View.VISIBLE
        } else if (passwordInput.equals("") == true) {
            passwordWarnning?.visibility = View.VISIBLE
            imgPasswordWarning?.visibility = View.VISIBLE
        } else if (confirmPasswordInput.equals("") == true
            || password?.text?.toString()?.trim().equals(passwordInput).not()) {
            confirmPasswordWarnning?.visibility = View.VISIBLE
            imgConfirmPasswordWarning?.visibility = View.VISIBLE
        } else {
            hideAllWarning()
            val auth = FirebaseAuth.getInstance()
            auth.createUserWithEmailAndPassword(usernameInput, passwordInput)
                .addOnCompleteListener(object : OnCompleteListener<AuthResult>{
                    override fun onComplete(task: Task<AuthResult>) {
                        if (task.isSuccessful) {
                            Toast.makeText(context, "Register success", Toast.LENGTH_LONG).show()
                            Handler(Looper.getMainLooper())
                                .postDelayed( Runnable {
                                    (context as MainActivity).onBackPressed()
                                }, 1500
                                )
                        } else {
                            Toast.makeText(context, "Account already exists", Toast.LENGTH_LONG).show()
                        }
                    }
                })

        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RegisterFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            RegisterFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}