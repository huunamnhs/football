package com.vn.footballtips.iap

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.android.billingclient.api.ProductDetails
import com.vn.footballtips.MainActivity
import com.vn.footballtips.R
import com.vn.footballtips.Utils.Utils

class IapAdapter(val context: Context): RecyclerView.Adapter<IapAdapter.ViewHolder>() {
    private var items = ArrayList<ProductDetails>()
    private var listener: IapListener? = null

    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: AppCompatTextView? = null
        var description: AppCompatTextView? = null
        var price: AppCompatTextView? = null

        init {
            title = itemView.findViewById(R.id.tv_title)
            description = itemView.findViewById(R.id.tv_description)
            price = itemView.findViewById(R.id.tv_price)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_iap, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items.get(position)
        item.let {
            holder.title?.text = item.name
            holder.description?.text = item.description
            holder.price?.text = item.oneTimePurchaseOfferDetails?.formattedPrice
            holder.itemView?.setOnClickListener(object : View.OnClickListener {
                override fun onClick(view: View?) {
                    if (Utils.isLogined()) {
                        listener?.onClick(items.get(holder.adapterPosition))
                    } else {
                        (context as MainActivity).openAccountScreen()
                    }
                }
            })
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setListener (listener: IapListener) {
        this.listener = listener
    }

    fun setData(list: ArrayList<ProductDetails>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }
}