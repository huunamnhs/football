package com.example.example

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class MatchFootball (

  @SerializedName("match_id"    ) var matchId      : Long?         = null,
  @SerializedName("leage"       ) var leage      : String?         = null,
  @SerializedName("home_flag"   ) var homeFlag      : String?         = null,
  @SerializedName("away_flag"   ) var awayFlag      : String?         = null,
  @SerializedName("home"        ) var home       : String?         = null,
  @SerializedName("away"        ) var away       : String?         = null,
  @SerializedName("time"        ) var time       : String?         = null,
  @SerializedName("final_score" ) var finalScore : String?         = null,
  @SerializedName("status" )      var status : Int?         = null,
  @SerializedName("tips"        ) var tips       : ArrayList<Tips> = ArrayList()

): Parcelable

fun MatchFootball.clone(): MatchFootball {
  val matchFootball = MatchFootball()
  matchFootball.matchId = matchId
  matchFootball.leage = leage
  matchFootball.home = home
  matchFootball.homeFlag = homeFlag
  matchFootball.awayFlag = awayFlag
  matchFootball.away = away
  matchFootball.time = time
  matchFootball.status = status
  matchFootball.finalScore = finalScore
  matchFootball.tips = ArrayList(tips)
  return matchFootball
}