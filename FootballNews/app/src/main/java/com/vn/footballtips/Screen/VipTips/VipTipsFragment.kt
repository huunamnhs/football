package com.vn.footballtips.Screen.VipTips

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.example.MatchFootball
import com.example.example.Tips
import com.example.example.clone
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vn.footballtips.MainActivity
import com.vn.footballtips.R
import com.vn.footballtips.Utils.Utils
import java.lang.reflect.Type

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Use the [VipTipsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class VipTipsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var recycleviewVip: RecyclerView? = null
    private var vipAdapater: VipAdapater? = null
    private var btnCredit: AppCompatButton? =null
    private var mapItemsUnlock = ArrayList<String>()//HashMap<String, String> ()
    private var detailFragment: DetailFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_vip_tips, container, false)
        mapItemsUnlock = Utils.historyList //getItemUnLock(context)
        initView(view)
        return view
    }

    private fun initView(view: View) {
        recycleviewVip = view.findViewById(R.id.recycleView_vip)

        context?.let {
//            showCredit(it)
            vipAdapater = VipAdapater(it)
            vipAdapater?.setOnListener(listener)
            val layoutManager = LinearLayoutManager(it, LinearLayoutManager.VERTICAL, false)
            recycleviewVip?.layoutManager = layoutManager
//            val divider = Utils.SpacesItemDecoration(25);
//            recycleviewVip?.addItemDecoration(divider)
            recycleviewVip?.adapter = vipAdapater
            processData()
        }


    }

    private fun showCredit(context: Context) {
        val mPrefs = context.getSharedPreferences("FootballPrefs", Context.MODE_PRIVATE)
        val account = mPrefs.getInt(Utils.eulaKey, 0)
        btnCredit?.text = account.toString().plus(" Credit")
    }

    fun processData() {
        val list = ArrayList<MatchFootball>()
        if (Utils.matchFootballs.isNotEmpty()) {
            for (item in Utils.matchFootballs) {
                val itemFootball = item.clone()
                val tips = itemFootball.tips
                val tipsTemp = ArrayList<Tips>()
                for (tip in tips) {
                    if (tip.coinfee != 0) {
                        tipsTemp.add(tip)
                    }
                }
                itemFootball.tips = ArrayList(tipsTemp)
                if (itemFootball.tips.isNotEmpty()) {
                    val time = item.time?.toLong()
                    var date = ""
                    if (time !=0L) {
                        if (time != null) {
                            date = DateFormat.format("dd MMMM yyyy",time * 1000).toString()
                        }
                    }
                    if (date.equals(Utils.choiceDate)) {
                        list.add(itemFootball)
                    }
                }
            }
        }
//        vipAdapater?.setItemsUnlock(mapItemsUnlock)
        vipAdapater?.setData(list)
    }

    fun addItemUnLock(context: Context?, name: String) {
        val items: HashMap<String, String> = getItemUnLock(context)
        items[name] = name
        val gson = Gson()
        val hashMapString = gson.toJson(items)
        val mPrefs = context?.getSharedPreferences("FootballPrefs", Context.MODE_PRIVATE)
        val editor = mPrefs?.edit();
        editor?.putString("unLock", hashMapString);
        editor?.commit()
    }

    fun getItemUnLock(context: Context?): HashMap<String, String> {
        val mapItems: HashMap<String, String>
        val mPrefs = context?.getSharedPreferences("FootballPrefs", Context.MODE_PRIVATE);
        val items =mPrefs?.getString("unLock", null)
        mapItems = if (items != null) {
            val gson = Gson()
            val type: Type = object : TypeToken<HashMap<String?, String?>?>() {}.type
            gson.fromJson(items, type)
        } else {
            HashMap()
        }
        return mapItems
    }

    val listener: VipListener = object : VipListener {
        override fun onClick(value: String) {
            super.onClick(value)
//            addItemUnLock(context, value)
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed(Runnable {
//                mapItemsUnlock.clear()
//                mapItemsUnlock = getItemUnLock(context)
//                vipAdapater?.setItemsUnlock(mapItemsUnlock)
                vipAdapater?.notifyDataSetChanged()
            }, 400)
        }

        override fun onUpdateCredit(credit: Int) {
            super.onUpdateCredit(credit)
            btnCredit?.text = credit.toString().plus(" Credit")
        }

        override fun onShowDetail(item: MatchFootball) {
            (context as MainActivity).openDetail(item)
//            (activity as MainActivity)?.let { act ->
//                detailFragment = DetailFragment.newInstance()
//                val bundle =  Bundle();
//                bundle.putParcelable("itemMatch", item)  // Key, value
//                detailFragment?.setArguments(bundle);
//                detailFragment?.let {
//                    act.supportFragmentManager.beginTransaction().apply {
//                        replace(R.id.container_fragment,it)
//                        addToBackStack(null)
//                        commit()
//                    }
//                }
//            }
        }
    }

    override fun onResume() {
        super.onResume()
//        context?.let {
//            showCredit(it)
//        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment VipTipsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            VipTipsFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}