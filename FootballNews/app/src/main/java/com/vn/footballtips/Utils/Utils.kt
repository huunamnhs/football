package com.vn.footballtips.Utils

import android.content.Context
import android.graphics.Rect
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.example.MatchFootball
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.vn.footballtips.MainActivity
import kotlin.collections.ArrayList


object Utils {
    var matchFootballs = ArrayList<MatchFootball>()
    val eulaKey = "acountCredit"
    val IAP_ITEM_1 = "football_item_1"
    val IAP_ITEM_2 = "football_item_2"
    val IAP_ITEM_3 = "football_item_3"
    val IAP_ITEM_4 = "football_item_4"
    val IAP_ITEM_5 = "football_item_5"
    var choiceDate = ""
    var credits  = 0
    var historyList = ArrayList<String>()
    val coinParam = "coin"
    val historyItemParam = "history_item"
    val db = FirebaseFirestore.getInstance()
    val auth = FirebaseAuth.getInstance()

    class SpacesItemDecoration(private val space: Int) : RecyclerView.ItemDecoration() {
        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            outRect.bottom = space

            // Add top margin only for the first item to avoid double space between items
            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = space
            }
        }
    }

    fun convertDpToPixel(dp: Int, context: Context): Int {
        return dp * (context.getResources()
            .getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT)
    }

    fun updateDataToFirebase() {
        auth.currentUser?.let {
            val docRef = db.collection("user").document(it.uid)
            val docData = hashMapOf("coin" to credits, "history_item" to if (historyList.size == 1) historyList.get(0) else historyList.joinToString())
            docRef.update(docData as Map<String, Any>)
        }

    }
    fun onDetectLogin(context: Context) {
        if (auth.currentUser != null) {
             getAccountInfo(context)
        }
    }

    fun isLogined(): Boolean {
        return auth.currentUser != null
    }

    private fun getAccountInfo(context: Context) {
        val docRef = auth.currentUser?.uid?.let { db.collection("user").document(it) }

        docRef?.addSnapshotListener { document, error ->
            if (error != null) {
                return@addSnapshotListener
            }
            if (document != null && document.data != null) {
                credits = document.data?.get(coinParam).toString().toInt()
                var history = document.data?.get(historyItemParam).toString()
                history = history.replace("\\s".toRegex(), "")
                if (history.equals("").not()) {
                    historyList = ArrayList(history.split(",").toList())
                }
            } else {
                credits = 0
                historyList = ArrayList()
                val docData = hashMapOf(coinParam to Utils.credits, historyItemParam to "")
                docRef.set(docData)
            }
            (context as MainActivity).setCredit()
        }
    }
}