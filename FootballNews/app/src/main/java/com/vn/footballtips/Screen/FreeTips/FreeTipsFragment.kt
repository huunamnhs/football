package com.vn.footballtips.Screen.FreeTips

import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.example.MatchFootball
import com.example.example.Tips
import com.example.example.clone
import com.vn.footballtips.R
import com.vn.footballtips.Utils.Utils
import com.vn.footballtips.Utils.Utils.choiceDate
import java.text.SimpleDateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match

/**
 * A simple [Fragment] subclass.
 * Use the [FreeTipsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FreeTipsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var recycleviewFree: RecyclerView? = null
    private var freeAdapater: FreeAdapater? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_free_tips, container, false)
        initView(view)
        return view
    }

    private fun initView(view: View) {
        recycleviewFree = view.findViewById(R.id.recycleView_free)
        context?.let {
            freeAdapater = FreeAdapater(it)
            val layoutManager = LinearLayoutManager(it, LinearLayoutManager.VERTICAL, false)
            recycleviewFree?.layoutManager = layoutManager
            recycleviewFree?.adapter = freeAdapater
            processData()
        }
    }

    fun processData() {
        val list = ArrayList<MatchFootball>()
        if (Utils.matchFootballs.isNotEmpty()) {
            for (item in Utils.matchFootballs) {
                val itemFootball = item.clone()
                val tips = itemFootball.tips
                val tipsTemp = ArrayList<Tips>()
                val tipsTemp1 = ArrayList<Tips>()
                var maxRate = 0.0
                var indexTip = 0
                for (tip in tips) {
                    if (tip.coinfee == 0) {
                        tipsTemp.add(tip)
                    }
                }
                if (tipsTemp.isNotEmpty()) {
                    for (index in 0..tipsTemp.size - 1) {
                        val item = tipsTemp.get(index)
                        item.rate?.let {
                            if (maxRate < it) {
                                maxRate = it
                                indexTip = index
                            }
                        }
                    }
                    tipsTemp1.add(tipsTemp.get(indexTip))
                }
                itemFootball.tips = ArrayList(tipsTemp1)

                if (itemFootball.tips.isNotEmpty()) {
                    val time = item.time?.toLong()
                    var date = ""
                    if (time !=0L) {
                        if (time != null) {
                            date = DateFormat.format("dd MMMM yyyy",time * 1000).toString()
                        }
                    }
                    if (date.equals(choiceDate)) {
                        list.add(itemFootball)
                    }
                }
            }
        }
        freeAdapater?.setData(list)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            FreeTipsFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}