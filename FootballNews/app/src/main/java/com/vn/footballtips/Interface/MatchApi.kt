package com.vn.footballtips.Interface

import com.example.example.MatchFootball
import retrofit2.Response
import retrofit2.http.GET
import java.util.*

interface MatchApi {
    @GET("tips")
    suspend fun getMatchFootball() : Response<ArrayList<MatchFootball>>
}