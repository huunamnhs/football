package com.vn.footballtips

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.billingclient.api.*
import com.vn.footballtips.Utils.Utils
import com.vn.footballtips.iap.IapAdapter
import com.vn.footballtips.iap.IapListener

class InAppPurchaseActivity : AppCompatActivity() {
    private var billingClient: BillingClient? = null
    private var recyclerViewIAP: RecyclerView? = null
    private var clostBtn: AppCompatImageView? = null
    private var iapAdapter: IapAdapter? = null
    val activity : Activity = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_in_app_purchase)
        initView()
        billingClient = BillingClient.newBuilder(this)
            .setListener(purchasesUpdatedListener)
            .enablePendingPurchases()
            .build()
        connectToGooglePlay()
    }

    private fun initView() {
        clostBtn = findViewById(R.id.clost_btn)
        clostBtn?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                finish()
            }
        })

        recyclerViewIAP = findViewById(R.id.recycleview_iap)
        iapAdapter = IapAdapter(this)
        iapAdapter?.setListener(listener = iapListener)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerViewIAP?.layoutManager = layoutManager
        val divider = Utils.SpacesItemDecoration(25);
        recyclerViewIAP?.addItemDecoration(divider)
        recyclerViewIAP?.adapter = iapAdapter
    }

    private val purchasesUpdatedListener =
        PurchasesUpdatedListener { billingResult, purchases ->
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
                for (purchase in purchases) {
                    handlePurchase(purchase)
                }
            }
        }

    private fun handlePurchase(purchase: Purchase) {
//        val mPrefs = activity.getSharedPreferences("FootballPrefs", Context.MODE_PRIVATE);
//        var credit = mPrefs.getInt(Utils.eulaKey, 0)
        if (purchase.products.isNotEmpty()) {
            when(purchase.products.get(0)) {
                Utils.IAP_ITEM_1 -> {
                    Utils.credits += 100
                    Utils.updateDataToFirebase()
                }
            }
//            val editor = mPrefs.edit();
//            editor.putInt(Utils.eulaKey, credit);
//            editor.apply()
        }
    }

    private fun connectToGooglePlay() {
        billingClient?.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode ==  BillingClient.BillingResponseCode.OK) {
                    getProductDetails()
                }
            }
            override fun onBillingServiceDisconnected() {
                connectToGooglePlay()
            }
        })
    }

    private fun getProductDetails() {
        val queryProductDetailsParams =
            QueryProductDetailsParams.newBuilder()
                .setProductList(
                    listOf(
                        QueryProductDetailsParams.Product.newBuilder()
                            .setProductId("power_item")
                            .setProductType(BillingClient.ProductType.INAPP)
                            .build()))
                .build()

        billingClient?.queryProductDetailsAsync(queryProductDetailsParams) {
                billingResult,
                productDetailsList ->
            if (billingResult?.responseCode == BillingClient.BillingResponseCode.OK && productDetailsList!= null) {
                val listItem = ArrayList<ProductDetails>()
                for (item in productDetailsList) {
                    listItem.add(item)
                }
                this@InAppPurchaseActivity.runOnUiThread(java.lang.Runnable {
                    iapAdapter?.setData(listItem)
                })
            }
        }
    }

    private val iapListener: IapListener = object : IapListener {
        override fun onClick(productDetails: ProductDetails) {
            val productDetailsParamsList = listOf(
                BillingFlowParams.ProductDetailsParams.newBuilder()
                    .setProductDetails(productDetails)
                    .build()
            )

            val billingFlowParams = BillingFlowParams.newBuilder()
                .setProductDetailsParamsList(productDetailsParamsList)
                .build()

            val billingResult = billingClient?.launchBillingFlow(activity, billingFlowParams)
        }
    }
}